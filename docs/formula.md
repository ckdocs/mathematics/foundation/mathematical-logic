# Formula

Other names:

-   **Formula**
-   **Expression**
-   **Well-Formed Formula**
-   **Propositional Formula**
-   **Logical Expression**
-   **Formal Expression**

Is a type of **Syntactic Formula** of **Variables** which is **Well-Formed** and by specifying variable values, it will give a **Truth Value**

$$
\begin{aligned}
    & (p \land q) \longrightarrow r
\end{aligned}
$$

---

## Types

There are two types of **Formulas** based on their **Breakability**:

-   **Atomic (Simple)**
-   **Compound (Complex)**

---

### Atomic (Simple)

A formula that **Cannot Break** into smaller formulas (**Has one atomic variable**)

-   $p = \texttt{Apple is red}$
-   $q = \texttt{5 is prime}$

**Atomic Formulas** will evaluate using **Set Theory**

---

### Compound (Complex)

A formula that **Can Break** into smaller formulas using **Logical Connectives**

-   $p \land q = \texttt{Weather is cold and cloudy}$
-   $p \longrightarrow q = \texttt{If you are a good boy, I will give you chocolate}$

**Compound Formulas** will evaluate using **Model Theory**

---

## Grammar

The **Syntax Grammar** for **Formulas (Expressions)** is as follows:

$$
\begin{aligned}
    & Expression \implies&& AtomicExpression \;|\; CompoundExpression
    \\
    \\
    & AtomicExpression \implies&& \top \;|\; \bot \;|\; P \;|\; Q \;|\; R \;|\; \dots
    \\
    \\
    & CompoundExpression \implies&& (Expression)
    \\
    & &&\;|\; [Expression]
    \\
    & &&\;|\; \neg Expression
    \\
    & &&\;|\; Expression \land Expression
    \\
    & &&\;|\; Expression \lor Expression
    \\
    & &&\;|\; Expression \longrightarrow Expression
    \\
    & &&\;|\; Expression \longleftrightarrow Expression
\end{aligned}
$$

---

## Analysis

### Truth Table

### Truth Tree

---

## Properties

We can analyse the **Possible Outcomes** of a **Formulas** by specifying all possible combinations of **Variables** values

There are some cases for **Possible Outcomes Range** of formulas:

-   **Tautology**: Always **True**
    -   $p \lor \neg p \equiv \top$
-   **Contingency**: Sometimes **True**, Sometimes **False**
    -   $p$
-   **Contradiction**: Always **False**
    -   $p \land \neg p \equiv \bot$
-   **Valid**: `Tautology`
-   **Invalid**: `Not Tautology`
-   **Falsifiable**: `Not Tautology`
-   **Unfalsifiable**: `Tautology`
-   **Satisfiable**: `Not Contradiction`
-   **Unsatisfiable**: `Contradiction`

![Properties](./assets/formula_properties.png)

-   **Tip**: **Contingency** means `0% < True 100%` and `0% < False < 100%` so both cases are possible and it't **Not Tautology** and **Not Contradiction**

---

### Tautology

---

### Contingency

---

### Contradiction

---

## Relations

The relationship between two or many **Formulas**

-   **Consistency**
-   **Entailment** = **Inference** = **Consequence** = **Validity**
-   **Equivalence**

### Consistency

### Equivalence

### Inference

---
