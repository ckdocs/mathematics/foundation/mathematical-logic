# Variable

Other names:

-   **Variable**
-   **Sentential Letter**
-   **Sentential Variable**
-   **Propositional Variable**

Is a **Truth-Valued Variable** (can be `true` or `false`) which contains the **Truth Value** of an **Atomic Proposition**, indicated by $P, Q, R, \dots$

$$
\begin{aligned}
    & p: \texttt{Water is blue} \equiv True
    \\
    & q: \texttt{Apple is hot} \equiv False
    \\
    & r: \texttt{2+2=4 and 3 is even} \equiv False
\end{aligned}
$$

---

## Truth Value

Other names:

-   **Truth Value**
-   **Logical Value**

Is a value indicating the **Relation** of a **Proposition** to **Truth**

Each **Logical System** has a range for possible **Truth Values** which specifies the **Power** of that logical system for **Understanding** the propositions

In **Mathematical Logic** the truth has two possible values, which can be **100% True** or **100% False**:

$$
\begin{aligned}
    & \texttt{True} \equiv 1 \equiv \top
    \\
    & \texttt{False} \equiv 0 \equiv \bot
\end{aligned}
$$

-   **Tip**: If everybody in world **Vote True** about a proposition it's **100% True**
-   **Tip**: Propositions with **0% < Vote < 100%** are **Not Proposition** in **Mathematical Logic**
    -   For supporting these sentences we can use a **Sigmoid Function** to declare an **Activation Border**
-   **Tip**: Propositions with **0% < Vote < 100%** are **Proposition** in **Fuzzy Logic**

---

## Evaluation

The process of **Converting** a **Variable** into a **Truth Value**, which involves all of the sciences:

-   **Set Theory**: `Sky is red` $\implies$ $False$
-   **Mathematics**: `2 + 2 = 4` $\implies$ $True$
-   **Physics**: `The G in earth is about 10` $\implies$ $True$
-   **Chemistry**: `Water structure is H2O2` $\implies$ $False$
-   etc

---
