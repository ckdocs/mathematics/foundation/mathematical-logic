# Mathematical Logic

Logic is the science of **Interpretation** of **Propositions** into **Truth Value** and **Reverse**, there are many **Logical Systems** depends on the **Truth Value Range**:

-   **0,1**: `Mathematical` Logic
-   **-1,0,1**: `Ternary` Logic
-   **0~1**: `Fuzzy` Logic
-   etc

The picture bellow contains the whole **Speech Recognition and Generation System** modules and concepts, the logic is the science of **Understanding/Generating Propositions** to/from **Mind Internal Model**:

![Logic](./assets/logic.png)

-   **Tip**: Any type of information can convert to **Propositions** using different systems, like **Audio**, **Video**, **Picture**, etc so we can **Understand any Information**
-   **Tip**: All of the **Speech Recognition and Generation System** modules are interpreters:
    -   **ASR**: $\texttt{Utterance}$ $\implies$ $\texttt{Sentence}$
    -   **TTS**: $\texttt{Sentence}$ $\implies$ $\texttt{Utterance}$
    -   **NLU**: $\texttt{Sentence}$ $\implies$ $\texttt{Proposition}$
    -   **NLG**: $\texttt{Proposition}$ $\implies$ $\texttt{Sentence}$
    -   **FLU**: $\texttt{Proposition}$ $\implies$ $\texttt{Truth Value}$
    -   **FLG**: $\texttt{Truth Value}$ $\implies$ $\texttt{Proposition}$

---

## Concepts

There are three **Levels of Abstractions** in **Speech Recognition and Generation System**:

-   **Utterance**: A sequence of **Sounds** that have some properties:
    -   **Tone**: `Gentle`, `Rough`, ...
    -   **Volume**: `Low`, `High`, ...
    -   etc
-   **Sentence**: A sequence of **Words** that have some properties:
    -   **Type**: `Declarative`, `Question`, `Command`
    -   **Language**: `Persian`, `English`, ...
    -   etc
-   **Proposition**: A **Formal Expression** that can be **Evaluated** into **Truth Value**:
    -   **Type**: `Atomic`, `Compound`

![Concepts](./assets/logic_concepts.png)

-   **Tip**: We can find **Many Equal Sentences** for **One Proposition**
-   **Tip**: We can find **Many Equal Utterances** for **One Sentence**
-   **Tip**: **Understanding** means **Interpreting** a **Proposition** into a **Truth Value**

---

## Example

I tell you `The weather is cold and mohammad is a good boy`, you will follow these steps inorder to understand my sentence:

-   You hear an **Utterance**, and you convert it to a **Sentence**:
    -   $$
        \begin{aligned}
            & \texttt{The weather is cold and mohammad is a good boy}
        \end{aligned}
        $$
-   You convert the **Sentence** into a **Proposition**:
    -   $$
        \begin{aligned}
            & P \land Q
            \\
            & P: \texttt{Weather is cold}
            \\
            & Q: \texttt{Mohammad is a good boy}
        \end{aligned}
        $$
-   You will **Evaluate** the **Proposition** using your **Mind Internal Model**:
    -   **Check** the weather from the window: `True (It's cold)`
    -   **Remeber** mohammads personality: `False (Not good)`
    -   **Evaluate** the value of the proposition `True & False = False`

---

## Types

In general there are two types of **Logical Systems**:

-   **Formal Logic**: We can write it on paper (`Syntax`)
-   **Informal Logic**: Cannot write on paper (`No syntax`)

There are many types of **Formal Logical Systems** or **Formal Systems** based on the **Value Range** or the **Power of Understanding**:

-   **Syllogistic Logic** (`Aristotle`)
-   **Transcendental Logic** (`Hegel`)
-   **Dialectical Logic** (`Kant`)
-   **Symbolic Logic** (`Mathematical - Classic`)
-   **Ternary Logic** (`Mathematical - Non-Classic`)
-   **Fuzzy Logic** (`Mathematical - Non-Classic`)

---

### Syllogistic Logic

**Syllogistic Logic** or **Traditional Logic** or **Term Logic** Is the oldest **Formal Logical System** invented by **Aristotle**

![Syllogistic Logic](./assets/syllogistic_logic.png)

---

### Symbolic Logic

**Symbolic Logic** or **Mathematical Logic** or **Binary Logic** or **Boolean Logic** is the newer version of `Syllogistic Logic` which is also **Binary Valued** (`0,1`)

![Symbolic Logic](assets/symbolic_logic.png)

---

### Ternary Logic

**Ternary Logic** is a **Three Valued** logic (`-1,0,1`)

![Ternary Logic](assets/ternary_logic.png)

-   **Tip**: We can find a logic with **Any** number of **Discrete Values** like this

---

### Fuzzy Logic

**Fuzzy Logic** is a **Real Valued** logic (`0~1`)

![Fuzzy Logic](assets/fuzzy_logic.png)

---

## Theories

Every **Logical System** must include four theories:

-   **Set Theory**: Evaluate **Variables**
-   **Model Theory**: Evaluate **Propositions**
-   **Proof Theory**: Proof **Properties** and **Relations** of **Formulas**
-   **Recursion Theory**: Complexity of **Evaluation** and **Proof**

---

### Set Theory

Is a branch of logic that deals with **Evaluation** of **Atomic Propositions** into **Truth Value**

This theory is the basics of **Understanding** using **Sets** and **Relations**

![Set Theory](./assets/set_theory.png)

---

### Model Theory

Is a branch of logic that deals with **Interpretation** of **Compound Propositions** into **Truth Value**

This theory has some dependencies on other mathematical branches:

-   **Automata Theory**
-   **Compiler Design**

![Model Theory](./assets/model_theory.png)

---

### Proof Theory

Is a branch of logic that deals with **Formal Proofs** using **Axioms** and **Inference Rules**

![Proof Theory](./assets/proof_theory.png)

---

### Recursion Theory

Is a branch of logic that deals with **Computability** and **Complexity** of evaluations

This theory has some dependencies on other mathematical branches:

-   **Data Structures**
-   **Algorithms**
-   **Automata Theory**

![Recursion Theory](./assets/recursion_theory.png)

---
