# Connectives

**Logical Operators** that act on **Atomic or Compound Propositions** to generate **More Complex Propositions**

---

## Operators

There many connective operators:

-   **Negation**: NOT
-   **Conjunction**: AND
-   **Negative AND**: NAND
-   **Disjunction**: OR
-   **Negative OR**: NOR
-   **Exclusive OR**: XOR
-   **Implication**: IF
-   **Biconditional**: IFF

---

### Negation (NOT)

**Negation** or **Complement** is a **Logical Operator** that the **result** will be **true** whenever **p** is **false**

There are many ways of representation in **Expressions**:

-   $\neg p$
-   $!\; p$

There are many ways of representation in **Statements**:

-   $\texttt{not p}$

![Negation](../assets/model_theory_connectives_negation.png)

-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & NOT(p) = 1 - p
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{it is not the case that 2 + 2 is equals to 4}: False
\end{aligned}
$$

---

### Conjunction (AND)

**Conjunction** is a **Logical Operator** that the **result** will be **true** whenever **p and q (Both)** are **true**

There are many ways of representation in **Expressions**:

-   $p \land q$
-   $p \cap q$
-   $p \;.\; q$

There are many ways of representation in **Statements**:

-   $\texttt{p and q}$
-   $\texttt{p but q}$

![Conjunction](../assets/model_theory_connectives_conjunction.png)

-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & AND(p, q) = min(p, q)
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{12 is divisable by 3 but 3 is a prime number}: True
\end{aligned}
$$

---

#### Consistency

---

### Negative AND (NAND)

**Negative AND** is a **Logical Operator** that the **result** will be **false** whenever **p and q (Both)** are **true**

There are many ways of representation in **Expressions**:

-   $p \uparrow q$

There are many ways of representation in **Statements**:

-   $\texttt{p nand q}$

![Negative AND](../assets/model_theory_connectives_negativeand.png)

-   **Tip**: $\text{NAND} = \text{NOT} + \text{AND}$
-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & NAND(p, q) = 1 - min(p, q)
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{Coffie or tea comes with dinner}: XOR
    \\
    & \texttt{You can pay using US dollars or euros}: XOR
    \\
    & \texttt{You can get a job when you learn C++ or Java}: XOR
\end{aligned}
$$

---

### Disjunction (OR)

**Disjunction** or **Inclusive OR** is a **Logical Operator** that the **result** will be **true** whenever **p or q or both (Atleast One)** are **true**

There are many ways of representation in **Expressions**:

-   $p \lor q$
-   $p \cup q$
-   $p + q$

There are many ways of representation in **Statements**:

-   $\texttt{p or q}$

![Disjunction](../assets/model_theory_connectives_disjunction.png)

-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & OR(p, q) = max(p, q)
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{16 - 4 = 10 or 4 is an even number}: True
\end{aligned}
$$

---

### Negative OR (NOR)

**Negative OR** is a **Logical Operator** that the **result** will be **false** whenever **p or q or both (Atleast One)** are **true**

There are many ways of representation in **Expressions**:

-   $p \downarrow q$

There are many ways of representation in **Statements**:

-   $\texttt{p nor q}$

![Negative OR](../assets/model_theory_connectives_negativeor.png)

-   **Tip**: $\text{NOR} = \text{NOT} + \text{OR}$
-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & NOR(p, q) = 1 - max(p, q)
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{Coffie or tea comes with dinner}: XOR
    \\
    & \texttt{You can pay using US dollars or euros}: XOR
    \\
    & \texttt{You can get a job when you learn C++ or Java}: XOR
\end{aligned}
$$

---

### Exclusive OR (XOR)

**Exclusive OR** is a **Logical Operator** that the **result** will be **true** whenever **only p or only q (Exactly One)** is **true**

There are many ways of representation in **Expressions**:

-   $p \overline{\lor} q$
-   $p \underline{\land} q$
-   $p \,\triangle\, q$
-   $p \oplus q$

There are many ways of representation in **Statements**:

-   $\texttt{p or q}$

![Exclusive OR](../assets/model_theory_connectives_exclusiveor.png)

-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & XOR(p, q) = (p \neq q) \;?\; 1 \;:\; 0
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{Coffie or tea comes with dinner}: XOR
    \\
    & \texttt{You can pay using US dollars or euros}: XOR
    \\
    & \texttt{You can get a job when you learn C++ or Java}: XOR
\end{aligned}
$$

---

### Implication (IF)

**Implication** or **Conditional** is a **Logical Operator** that the **result** will be **true** whenever **p** is **false** or **p and q** are **true**

There are many ways of representation in **Expressions**:

-   $p \longrightarrow q$

There are many ways of representation in **Statements**:

-   $\texttt{if p then q}$
-   $\texttt{p implies q}$
-   $\texttt{p only if q}$
-   $\texttt{p is sufficient for q}$
-   $\texttt{q when p}$
-   $\texttt{q whenever p}$
-   $\texttt{q follows from p}$
-   $\texttt{q is necessary for p}$
-   $\texttt{q unless !p}$

![Implication](../assets/model_theory_connectives_implication.png)

-   **Tip**: The **p** called **Hypothesis** or **Premise**
-   **Tip**: The **q** called **Conclusion** or **Consequence**
-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & IF(p, q) = (p \leq q) \;?\; 1 \;:\; 0
        \end{aligned}
        $$

---

**Example**: There are some examples

$$
\begin{aligned}
    & \texttt{If you try hard for your exam, then you will success}
    \\
    & \texttt{If we won't have good food then we'll soon die}
    \\
    & \texttt{Good food is necessary to keep us alive}
\end{aligned}
$$

---

#### Types

There are **Four** types of **Implications**:

$$
\begin{aligned}
    & \texttt{Implication}:&& p \longrightarrow q
    \\
    & \texttt{Contrapositive}:&& \neg q \longrightarrow \neg p
    \\
    & \texttt{Converse}:&& q \longrightarrow p
    \\
    & \texttt{Inverse}:&& \neg p \longrightarrow \neg q
\end{aligned}
$$

![Implication Types](../assets/model_theory_connectives_implication_types.png)

-   **Tip**: (**Implication** $\equiv$ **Contrapositive**) $\not\equiv$ (**Converse** $\equiv$ **Inverse**)

---

#### Logical Inference

---

### Biconditional (IFF)

**Biconditional** is a **Logical Operator** that the **result** will be **true** whenever **p and q** are **equal**

There are many ways of representation in **Expressions**:

-   $p \longleftrightarrow q$

There are many ways of representation in **Statements**:

-   $\texttt{p iff q}$
-   $\texttt{p if and only if q}$
-   $\texttt{if p then q and conversely}$
-   $\texttt{p is necessary and sufficient for q and viceversa}$

![Biconditional](../assets/model_theory_connectives_biconditional.png)

-   **Tip**: There is an equivalent **Numeric Function** from values **0,1**:
    -   $$
        \begin{aligned}
            & IFF(p, q) = (p = q) \;?\; 1 \;:\; 0
        \end{aligned}
        $$

---

**Example**: There are some examples

---

#### Logical Equivalence

---

## Presidence

The **Priority** of **Applying** operations in a **Logical Expression** in order to generate correct **Abstract Syntax Tree** for correct evaluation

1. $()$
2. $\neg$
3. $\land$
4. $\lor$
5. $\longrightarrow$
6. $\longleftrightarrow, \oplus$

![Presidence](../assets/model_theory_connectives_presidence.png)

---

## Associativity

The **Direction** of **Applying** operations in a **Logical Expression** whenever the **Same Operators** exists

-   $()$: **Left to Right**
-   $\neg$: **Right to Left**
-   $\land$: **Left to Right**
-   $\lor$: **Left to Right**
-   $\longrightarrow$: **Left to Right**
-   $\longleftrightarrow, \oplus$: **Left to Right**

---

## Functional Completeness

A set of **Operators** which can generate all other **AND, OR, NOT Operators**

-   $\{\neg, \land, \lor\}$
-   $\{\neg, \land\}$
-   $\{\neg, \lor\}$
-   $\{\neg, \longrightarrow\}$
-   $\{\uparrow\}$
-   $\{\downarrow\}$

---
