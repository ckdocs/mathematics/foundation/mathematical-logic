# Model Theory

Is a branch of every **Logical System** that deals with **Interpretation** of **Compound Propositions** to a **Truth Value (0,1)**

![Interpreter](../assets/model_theory_interpreter.png)

There are two **Branches** in model theory:

-   **Propositional Logic**
-   **Predicate Logic**

---

## Propositional Logic

**Propositional Logic** or **Propositional Calculus** or **Sentential Logic** or **Statement Logic** or **Zeroth Order Logic** is a branch that deals with interpretation of **Propositional Formulas (Propositions (true, false))**

---

### Proposition (Operand)

Is a **Propositional Formula** with **Propositional Variables** that can be **Evaluated** into **Truth Value** which can be **100% True** or **100% False**

**Example**:

-   **1 + 1 = 2**: `100% true declare` $\implies$ proposition
-   **Weather is cold**: `100% false declare` $\implies$ proposition
-   **I'm a good boy**: `30% true declare` $\implies$ not proposition
-   **X + 1 = 2**: `50% true declare` $\implies$ not proposition
-   **UFO's are exists**: `not enough knowledge` $\implies$ not proposition
-   **What is time?**: `query` $\implies$ not proposition
-   **Do the dishes!**: `command` $\implies$ not proposition

---

### Connective (Operator)

**Logical Operators** that act on **Atomic or Compound Propositions** to generate **More Complex Propositions**, there are many types of connectives:

![Connective](../assets/model_theory_connective.png)

---

## Predicate Logic

**Predicate Logic** or **Predicate Calculus** or **First/Second/etc Order Logic** is a branch that deals with interpretation of **Propositional Functions (Predicates (variable))**

---

### Predicate (Function)

Is an **Indeterminate (Pseudo-Proposition)** which contains **Free Variables**, and by specifying them, function will return a **Proposition**

-   **Tip**: The `variables` and `quantifiers` first invented by **Aristotle**
-   **Tip**: Each predicate consists of two parts:
    -   **Subject**: The variable
    -   **Statement**: The predicate sentence
    -   $$
        \begin{aligned}
            & F(x): \underbrace{\overbrace{x}^{\text{Subject}} \texttt{is an animal}}_{\text{Statement}}
            \\ \implies
            & \begin{cases}
                F(Cat): \texttt{Cat is an animal} \implies \text{True proposition}
                \\
                F(Car): \texttt{Car is an animal} \implies \text{False proposition}
            \end{cases}
        \end{aligned}
        $$

**Example**:

-   **F(x): x + 1 = 2** $\implies$ $F(1): True, F(2): False$
-   **F(x): x is less than 4** $\implies$ $F(3): True, F(4): False$
-   **F(x,y): x is less than y** $\implies$ $F(3,4): True, F(4,4): False$
-   **F(x): x is an animal** $\implies$ $F(Cat): True, F(Car): False$
-   **G(p,q): p <=> q**

---

### Quantifier (Iterator)

**Logical Iterator** that iterate over **Set of Objects** or **Quantities** and check **Value of Predicate**, there are many types of quantifiers:

![Quantifier](../assets/model_theory_quantifier.png)

---
