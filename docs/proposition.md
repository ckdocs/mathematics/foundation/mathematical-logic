# Proposition

Other names:

-   **Proposition**

Is a tuple of **Formula** and **Variables** that can be **Evaluated** into **Truth Value**

![Proposition](./assets/proposition.png)

---

## Declaration

There are two ways of declaring **Propositions**:

-   **Formal (Expression)**: Declaring **Formula** and **Variables** seperately in **Mathematical Format**
-   **Informal (Statement)**: Writing a **Declarative Sentence** that contains **Variables**

---

### Formal (Expression)

**Mathematical** declaration of propositions, by specifying **Propositional Formula (Expression)** and **Propositional Variables** seperately

$$
\begin{aligned}
    & p \land q
    \\
    & p: \texttt{Water is blue}
    \\
    & p: \texttt{Sky is blue}
\end{aligned}
$$

---

### Informal (Statement)

**Sentential** declaration of propositions, using a **Declarative Sentence** which contains **Propositional Formula (Expression)** and **Propositional Variables**

$$
\begin{aligned}
    & \texttt{Water is blue and Sky is blue}
\end{aligned}
$$

---

## Evaluation

The process of **Converting** a **Proposition** into a **Truth Value**, which has some steps:

-   **Lexer**: Generating a **Sequence of Tokens** from proposition
-   **Parser**: Generating an **AST (Abstract Syntax Tree)** from sequence of tokens
-   **Evaluator**: Traversing the **AST** and evaluate each **Atomic Node** and **Aggregate** values

There are two methods of evaluation:

-   **Lazy Evaluation**
-   **Eager Evaluation**

---

### Lazy

Is an evaluation strategy which only needed **Atomic Propositions** evaluated and **Aggregated**

$$
\begin{aligned}
    & P \land Q \land R
    \\
    \\ \implies
    & Eval(P): True
    \\
    & Eval(Q): False
    \\
    & \texttt{Break and return False}
\end{aligned}
$$

---

### Eager

Is an evaluation strategy which all **Atomic Propositions** evaluated and **Aggregated**

$$
\begin{aligned}
    & P \land Q \land R
    \\
    \\ \implies
    & Eval(P): True
    \\
    & Eval(Q): False
    \\
    & Eval(R): True
    \\
    & \texttt{Aggregate and return False}
\end{aligned}
$$

---

## Function

Other names:

-   **Propositional Form**
-   **Propositional Function**
-   **Logical-Valued Function**
-   **Predicate Formula**
-   **Predicate**

Is an **Indeterminate (Pseudo-Proposition)** which contains **Free Variables**, and by specifying them, function will return a **Proposition**

$$
\begin{aligned}
    & \lambda (x, y, z, \dots) \mapsto Proposition
\end{aligned}
$$

---

**Example**:

-   **F(x): x + 1 = 2** $\implies$ $F(1): 1 + 1 = 2 \equiv True$
-   **F(x): x is less than 4** $\implies$ $F(4): \texttt{4 is less than 4} \equiv False$
-   **F(x,y): x is less than y** $\implies$ $F(3,4): \texttt{3 is less than 4} \equiv True$
-   **F(x): x is an animal** $\implies$ $F(Cat): \texttt{Cat is an animal} \equiv True$
-   **G(p,q): p <=> q**

---
