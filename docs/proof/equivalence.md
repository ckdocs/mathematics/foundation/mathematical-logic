# Equivalence

## Duality

---

## Laws

![Laws](../assets/proof_theory_equivalence_laws.png)

---

### Identity

$$
\begin{aligned}
    & p \land \top \equiv p
    \\
    & p \lor \bot \equiv p
\end{aligned}
$$

---

### Domination

$$
\begin{aligned}
    & p \land \bot \equiv \bot
    \\
    & p \lor \top \equiv \top
\end{aligned}
$$

---

### Idempotency

$$
\begin{aligned}
    & p \land p \equiv p
    \\
    & p \lor p \equiv p
\end{aligned}
$$

---

### Commutative

$$
\begin{aligned}
    & p \land q \equiv q \land p
    \\
    & p \lor q \equiv q \lor p
\end{aligned}
$$

---

### Associative

$$
\begin{aligned}
    & p \land (q \land r) \equiv (p \land q) \land r \equiv p \land q \land r
    \\
    & p \lor (q \lor r) \equiv (p \lor q) \lor r \equiv p \lor q \lor r
    \\
    & p \oplus (q \oplus r) \equiv (p \oplus q) \oplus r \equiv p \oplus q \oplus r
    \\
    & p \longleftrightarrow (q \longleftrightarrow r) \equiv (p \longleftrightarrow q) \longleftrightarrow r \equiv p \longleftrightarrow q \longleftrightarrow r
    \\
    \\
    & p \uparrow (q \uparrow r) \not\equiv (p \uparrow q) \uparrow r \not\equiv p \uparrow q \uparrow r
    \\
    & p \downarrow (q \downarrow r) \not\equiv (p \downarrow q) \downarrow r \not\equiv p \downarrow q \downarrow r
    \\
    & p \longrightarrow (q \longrightarrow r) \not\equiv (p \longrightarrow q) \longrightarrow r \not\equiv p \longrightarrow q \longrightarrow r
\end{aligned}
$$

---

### Distributive

$$
\begin{aligned}
    & p \land (q \lor r) \equiv (p \land q) \lor (p \land r)
    \\
    & p \lor (q \land r) \equiv (p \lor q) \land (p \lor r)
\end{aligned}
$$

---

### Absorption

$$
\begin{aligned}
    & p \land (p \lor q) \equiv p
    \\
    & p \lor (p \land q) \equiv p
\end{aligned}
$$

---

### Ellimination

$$
\begin{aligned}
    & p \land (\neg p \lor q) \equiv p \land q
    \\
    & p \lor (\neg p \land q) \equiv p \land q
\end{aligned}
$$

---

### Double Negation

$$
\begin{aligned}
    & \neg (\neg p) \equiv p
\end{aligned}
$$

---

### DeMorgans

$$
\begin{aligned}
    & \neg (p \land q) \equiv \neg p \lor \neg q
    \\
    & \neg (p \lor q) \equiv \neg p \land \neg q
\end{aligned}
$$

---

### Negation

$$
\begin{aligned}
    & p \land \neg p \equiv \bot
    \\
    & p \lor \neg p \equiv \top
\end{aligned}
$$

---

### Conditional

$$
\begin{aligned}
    & p \longrightarrow q \equiv \neg p \lor q
    \\
    & p \longrightarrow q \equiv \neg q \longrightarrow \neg p
    \\
    & p \lor q \equiv \neg p \longrightarrow p
    \\
    & p \land q \equiv \neg (q \longrightarrow \neg p)
    \\
    & \neg (p \longrightarrow q) \equiv p \land \neg q
    \\
    & (p \longrightarrow q) \land (p \longrightarrow r) \equiv p \longrightarrow (q \land r)
    \\
    & (p \longrightarrow r) \land (q \longrightarrow r) \equiv (p \lor q) \longrightarrow r
    \\
    & (p \longrightarrow q) \lor (p \longrightarrow r) \equiv p \longrightarrow (q \lor r)
    \\
    & (p \longrightarrow r) \lor (q \longrightarrow r) \equiv (p \land q) \longrightarrow r
\end{aligned}
$$

---

### Biconditional

$$
\begin{aligned}
    & p \longleftrightarrow q \equiv (p \longrightarrow q) \land (q \longrightarrow p)
    \\
    & p \longleftrightarrow q \equiv \neg p \longleftrightarrow \neg q
    \\
    & p \longleftrightarrow q \equiv (p \land q) \lor (\neg p \land \neg q)
    \\
    & p \longleftrightarrow q \equiv \neg (p \oplus q)
\end{aligned}
$$

---
