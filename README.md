# Mathematical Logic

![Date](https://img.shields.io/badge/BeginDate-2/26/2021-success)

Mathematical Logic

---

## Dependencies

1. `docker`

## Building

```sh
docker run --rm -it -v "$(pwd):/docs" squidfunk/mkdocs-material build
```

## Starting

```sh
docker run --rm -it -p 8000:8000 -v "$(pwd):/docs" squidfunk/mkdocs-material
```

---

## License

This project is licensed under the [MIT license](LICENSE.md).  
Copyright (c) KoLiBer (koliberr136a1@gmail.com)
